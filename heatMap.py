import plotly.offline as py
import plotly.graph_objs as go
import sqlite3
import numpy as np

#Leer la base de datos
conn = sqlite3.connect("database.db")
cur = conn.cursor()
cur.execute("SELECT Date, ZONE FROM my_Accidents_2014_Scotland GROUP BY Date ORDER BY substr(Date, 7,4) || substr(Date, 4, 2)|| substr(Date, 1, 2) ASC")
data = np.array([list(x) for x in cur.fetchall()])
conn.close()

#Dias
x = data[:,0]
#Numero de grupos o id identificativo
y = range(32)
#Zonas (el 0 representa el ruido)
z = [d if d else 0 for d in data[:,1]]

#Plot
plot_url = py.plot([go.Heatmap(x=x, y=y, z=z)], filename='Annotated Heatmap.html')
