#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import threading
import sqlite3
import collections
import numpy as np
from mpl_toolkits.basemap import Basemap
from sklearn.cluster import DBSCAN
from _sqlite3 import OperationalError
from scipy.spatial import ConvexHull
from matplotlib.patches import Polygon


EPS_DEFAULT = 0.02
MIN_SAMPLES_DEFAULT = 20
RATIO_PRECISION = 3


def my_dbscan(eps, min_samples, data):
    # Compute DBSCAN
    # http://scikit-learn.org/stable/modules/generated/sklearn.cluster.DBSCAN.html
    # http://scikit-learn.org/stable/auto_examples/cluster/plot_dbscan.html
    labels = DBSCAN(eps, min_samples).fit_predict(data)
    print labels
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    print('Estimated number of clusters: %d' % n_clusters_)
    return labels


def plotmap(tablename, labels, data):
    m = Basemap(width=300000, height=400000, resolution='h', projection='aea', lon_0=-4, lat_0=56.5)
    #m = Basemap(width=600000, height=900000, resolution='h', projection='aea', lon_0=-2, lat_0=53)

    m.drawmapboundary(fill_color='aqua')
    m.fillcontinents(color='coral', lake_color='aqua')

    parallels = np.arange(0., 81, 10.)
    m.drawparallels(parallels, labels=[False, True, True, False])
    meridians = np.arange(0., 351., 10.)
    m.drawmeridians(meridians, labels=[True, False, False, True])

    unique_labels = set(labels)
    unique_labels.remove(-1)
    colors = plt.cm.get_cmap('jet')(np.linspace(0, 1, len(unique_labels)))
    for k, col in zip(unique_labels, colors):
        xy = data[labels == k]
        x, y = m(xy[:, 0], xy[:, 1])
        
        points = np.array([[x,y] for x, y in zip (x, y)])
        hull = ConvexHull(points)
        polygon = [[x,y] for x, y in zip(points[hull.vertices,0], points[hull.vertices,1])]
        plt.gca().add_patch(Polygon(polygon, facecolor=col, alpha=0.8))

    plt.title(tablename)
    plt.show()


def PolygonArea(corners):
    n = len(corners) # of corners
    area = 0.0
    for i in range(n):
        j = (i + 1) % n
        area += corners[i][0] * corners[j][1]
        area -= corners[j][0] * corners[i][1]
    area = abs(area) / 2.0
    return area


def storeZones(conn, tablename, labels, data):
    cur = conn.cursor()
    unique_labels = set(labels)
    unique_labels.remove(-1)
    
    try:
        cur.execute('ALTER TABLE ' + tablename + ' ADD COLUMN ZONE integer')
    except OperationalError:
        cur.execute('UPDATE ' + tablename + ' SET ZONE=NULL')
    
    zoneTable = tablename + '_ZONES' 
    try:
        cur.execute('DROP TABLE ' + zoneTable)
    except OperationalError:
        pass
    cur.execute('CREATE TABLE ' + zoneTable + ' (id integer, ' + 
                'area real, ' +
                'total_accidents integer, ' +
                'vehicles_Median real, ' +
                'casualties_Median real, ' +
                'speedLimit_Median real, ' +
                'dayOfWeek_Mode integer, ' +
                'urbanRural_Ratio real, ' +
                'inJunction_Ratio real, ' +
                'lightConditions_Ratio real, ' +
                'weatherConditions_Ratio real, ' +
                'highWinds_Ratio real, ' +
                'roadSurfaceConditions_Ratio real, ' +
                'severity integer)')
        
    for i, k in enumerate(unique_labels, start=1):
        print 'Saving Cluster ' + str(i) + '\r',
        # Zone number
        xy = data[labels == k]
        x = tuple([str(n) for n in xy[:, 0]])
        y = tuple([str(n) for n in xy[:, 1]])
        cur.execute('UPDATE ' + tablename + ' SET ZONE=' + str(i) +
                    ' WHERE Longitude IN ' + str(x) +
                    ' AND Latitude IN ' + str(y))
        conn.commit()
        cur.execute('SELECT * FROM ' + tablename + ' WHERE ZONE=' + str(i))
        Zpoints = cur.fetchall()

        # area
        aea = Basemap(width=300000, height=400000, resolution=None, projection='aea', lon_0=-4, lat_0=56.5)
        x, y = aea(xy[:, 0], xy[:, 1])
        points = np.array([[x,y] for x, y in zip (x, y)])
        hull = ConvexHull(points)
        polygon = [[x,y] for x, y in zip(points[hull.vertices,0], points[hull.vertices,1])]
        area = PolygonArea(polygon) / 1000; # Km
        area = round(area, 2)
        
        # casualties
        casualties = np.array([row[8] for row in Zpoints])
        casualties_Median = np.median(casualties)

        # accidents_Total
        accidents_Total = len(Zpoints)

        # severity
        severity_Mode = np.average(np.asarray([row[6] for row in Zpoints]))
        severity_Mode = round(severity_Mode, RATIO_PRECISION)

        # vehicles
        vehicles_Median = np.median(np.asarray([row[7] for row in Zpoints]))

        # dayofweek
        dayOfWeek_Mode = collections.Counter([row[10] for row in Zpoints]).most_common()[0][0]

        # speedLimit
        speedLimit_Median = np.median(np.asarray([row[17] for row in Zpoints]))

        # roadSurfaceConditions
        surfaceConditions = [row[26] for row in Zpoints if row[26] > 0]
        roadSurfaceConditions_Ratio = surfaceConditions.count(1) / float(len(surfaceConditions))
        roadSurfaceConditions_Ratio = round(roadSurfaceConditions_Ratio, RATIO_PRECISION)

        # lightConditions
        lightConditions = [row[24] for row in Zpoints if row[24] > 0]
        lightConditions_Ratio = lightConditions.count(1) / float(len(lightConditions))
        lightConditions_Ratio = round(lightConditions_Ratio, RATIO_PRECISION)

        # weatherConditions
        weatherConditions = [row[25] for row in Zpoints if row[25] > 0 and row[25] < 9]
        weatherConditions_Ratio = weatherConditions.count(1) / float(len(weatherConditions))
        weatherConditions_Ratio = round(weatherConditions_Ratio, RATIO_PRECISION)

        # highWinds
        highWinds = [x for x in weatherConditions if x in [4,5,6]]
        highWinds_Ratio = len(highWinds) / float(len(weatherConditions))
        highWinds_Ratio = round(highWinds_Ratio, RATIO_PRECISION)

        # urbanRural
        urbanRural = [row[29] for row in Zpoints if row[29] < 3]
        urbanRural_Ratio = urbanRural.count(1) / float(len(urbanRural))
        urbanRural_Ratio = round(urbanRural_Ratio, RATIO_PRECISION)

        # inJunction
        junction = [row[18] for row in Zpoints if row[18] >= 0]
        inJunctionCount = len(junction) - junction.count(0)
        inJunction_Ratio = inJunctionCount / float(len(junction))
        inJunction_Ratio = round(inJunction_Ratio, RATIO_PRECISION)

        zone = (i,
                area,
                accidents_Total,
                vehicles_Median,
                casualties_Median,
                speedLimit_Median,
                dayOfWeek_Mode,
                urbanRural_Ratio,
                inJunction_Ratio,
                lightConditions_Ratio,
                weatherConditions_Ratio,
                highWinds_Ratio,
                roadSurfaceConditions_Ratio,
                severity_Mode)
        cur.execute('INSERT INTO ' + zoneTable +' VALUES ' + str(zone))
    print
    conn.commit()


def main(tablename, eps=EPS_DEFAULT, min_samples=MIN_SAMPLES_DEFAULT, plot=False):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute('SELECT Longitude, Latitude FROM ' + tablename)
    data = np.array([list(x) for x in cur.fetchall()])
    
    # DBSCAN
    labels = my_dbscan(eps, min_samples, data)
    
    # plotting & saving
    if plot:
        threading.Thread(target=plotmap, args=(tablename, labels, data)).start()
    storeZones(conn, tablename, labels, data)

    conn.close()
