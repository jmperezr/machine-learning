#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""
Autor: Basilio Carrero Nevado.
"""
from timeit import itertools
import sys
import time
import os
import sqlite3
import csv
import glob
import codecs
import re


class regType(object):
    TEXT = "text"
    INTGR = "integer"
    REAL = "real"


def numberType(s):
    try:
        int(s)
        return regType.INTGR
    except ValueError:
        try:
            float(s)
            return regType.REAL
        except ValueError:
            return regType.TEXT


def create_table(c, tableName, colNames, data):
    colType = ""
    for i, col in enumerate(colNames):
        colType += '"%s" %s,' % (col, numberType(data[i]))

    sql = 'CREATE TABLE ' + tableName + ' (' + colType[:-1] + ')'
    c.execute(sql)
    return sql


def getDelimiter(s):
    delimiters = re.match(ur'[\w"\'()-]+([,;]?)', s)
    if delimiters:
        return delimiters.groups()[0].encode('ascii', 'ignore')
    else:
        return raw_input('¿Delimitador del CSV? ')


def createTablesFromCSVfiles(conn, fileList):
    BUFFSIZE = 400000
    c = conn.cursor()
    c.execute('PRAGMA synchronous = OFF')
    c.execute('PRAGMA journal_mode = OFF')
    c.execute('PRAGMA temp_store = MEMORY')
    c.execute('PRAGMA page_size = 4096')

    for f in fileList:
        with codecs.open(f, 'rb', 'utf-8-sig') as csvfile:
            fileName = csvfile.name.split('.')[0]
            tableName = "my_" + fileName
            delimiter = getDelimiter(next(csvfile))

            csvfile.seek(0)
            spamreader = csv.reader(csvfile, delimiter=delimiter)
            colNames = next(spamreader)
            fullRow = None
            for row in spamreader:
                if "" not in row:
                    fullRow = row
                    break

            if fullRow:
                print create_table(c, tableName, colNames, fullRow)

                csvfile.seek(0)
                spamreader = csv.reader(csvfile, delimiter=delimiter)
                next(spamreader)
                rowsChunk = []
                while True:
                    it = itertools.islice(spamreader, BUFFSIZE)
                    rowsChunk = [tuple(x) for x in it]
                    if not rowsChunk:
                        print "Done\n"
                        break
                    else:
                        aux = '?,' * len(rowsChunk[0])
                        sqlInsert = "INSERT INTO " + tableName
                        sqlInsert += " VALUES (" + aux[:-1] + ")"
                        c.executemany(sqlInsert, rowsChunk)
                    print '.',
            else:
                print "Full row not found, skipping table..."
    
    conn.commit()


def main(files):
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    sys.stderr = codecs.getwriter('utf8')(sys.stderr)

    try:
        os.remove('database.db')
    except OSError:
        pass
    
    try:
        conn = sqlite3.connect('database.db')
        start = time.clock()
        createTablesFromCSVfiles(conn, files)
        print ("Time = %.2fs" % round(time.clock() - start, 2))
    except KeyboardInterrupt:
        # handle keyboard interrupt #
        return 0
    except Exception, e:
        print e
        return 2
    finally:
        conn.close()


if __name__ == "__main__":
    main(glob.glob("*.csv"))
