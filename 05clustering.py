#!/usr/bin/env python
# -*- coding: utf-8 -*-

import codecs
import matplotlib.pyplot as plt
from matplotlib.pylab import hist, show
import numpy
import scipy
import scipy.spatial
#http://docs.scipy.org/doc/scipy/reference/cluster.html
from scipy import cluster

# http://scikit-learn.org/stable/modules/preprocessing.html
from sklearn import preprocessing 
from scipy.spatial.distance import cdist

import sklearn.neighbors



# 0. Load Data
f = codecs.open("RoadSafetyPivot.csv", "r", "utf-8")
states = []
count = 0
for line in f:
	if count > 0: 
		# remove double quotes
		row = line.replace ('"', '').split(",")
		row.pop(0)
		if row != []:
			states.append(map(float, row))
	count += 1
	
# 1. Compute the similarity matrix
dist = sklearn.neighbors.DistanceMetric.get_metric('euclidean')
matsim = dist.pairwise(states)
avSim = numpy.average(matsim)

# 3. Building the Dendrogram	
# http://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html#scipy.cluster.hierarchy.linkage
clusters = cluster.hierarchy.linkage(matsim, method = 'single')
#clusters = cluster.hierarchy.linkage(matsim, method = 'complete')
#clusters = cluster.hierarchy.linkage(matsim, method = 'ward')
cluster.hierarchy.dendrogram(clusters, color_threshold=0)
plt.show()


# 4. Cutting the dendrogram
#Forms flat clusters from the hierarchical clustering defined by the linkage matrix Z
clusters = cluster.hierarchy.fcluster(clusters, avSim, criterion = 'distance')
print clusters

