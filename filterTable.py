#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3
import sys
import reverse_geocoder as rg
from createDatabase import create_table


def admin1_filter(tableList, op=None):
    # read data
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    
    for table in tableList:
        cur.execute('SELECT * FROM ' + table)
        colNames = [description[0] for description in cur.description]
        data = [list(x) for x in cur.fetchall()]
    
        # filter data
        res = {}
        loc = rg.search([(d[4], d[3]) for d in data])
        for l, d in zip(loc, data):
            try:
                res[l['admin1']].append(tuple(d))
            except Exception:
                res[l['admin1']] = []
                res[l['admin1']].append(tuple(d))
        
        if op:
            op.title()
            if op not in res.keys():
                print 'Filtro incorrecto.'
                conn.close()
                sys.exit(1)
        else:
            while op not in res.keys():
                print res.keys()
                op = raw_input('Ingrese una estado para filtrar: ')
                op = op.title()
    
        newTableName = table + '_' + op
        res = res[op]
    
        # writeback
        row = None
        for d in data:
            if '' not in d:
                row = d
                break
        if row:
            print create_table(cur, newTableName, colNames, row)
            aux = '?,' * len(res[0])
            sqlInsert = "INSERT INTO " + newTableName
            sqlInsert += " VALUES (" + aux[:-1] + ")"
            cur.executemany(sqlInsert, res)
            conn.commit()

    print 'Done!'
    conn.close()
