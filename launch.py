#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import glob
import createDatabase
import filterTable
import dbscan

TABLE = 'my_Accidents_2013_Scotland'
FILTER = 'Scotland'
EPS_DEFAULT = 0.02
MIN_SAMPLES_DEFAULT = 20


if __name__ == "__main__":
    # create DB from CSV if not exist
    if not os.path.isfile('database.db'):
        files = glob.glob("Accidents*.csv")
        createDatabase.main(files)
        tables = ['my_' + f.split('.')[0] for f in files]
        filterTable.admin1_filter(tables, FILTER)


    # parameters
    eps = raw_input('eps (default=%s):' % EPS_DEFAULT)
    eps = float(eps) if eps != '' else EPS_DEFAULT
    min_samples = raw_input('min_samples (default=%s):' % MIN_SAMPLES_DEFAULT)
    min_samples = int(min_samples) if min_samples != '' else MIN_SAMPLES_DEFAULT
    plotYN = raw_input('Plot? (y/n): ')
    plotYN = True if plotYN == 'y' else False
    # dbScan
    dbscan.main(TABLE, eps, min_samples, plotYN)
