import csv
import sys

f = open("DfTRoadSafetyAccidents.csv", 'rt')
estados = []
est = ""
try:
    reader = csv.reader(f)
    datos = None
    accidentes2014= 0
    accidentes2013= 0
    accidentes2012= 0
    for row in reader:
    	newest = row[13]
    	if est == newest:
		if row[9].rfind("2014") == 6:
    			accidentes2014 += 1	
		elif row[9].rfind("2013") == 6:
			accidentes2013 += 1
		elif row[9].rfind("2012") == 6:
			accidentes2012 += 1
    	else:    
    		if datos != None:
			datos.append(accidentes2014+1)
			datos.append(accidentes2013)
			datos.append(accidentes2012)
    			estados.append(datos)
		accidentes2014= 0
		accidentes2013= 0
		accidentes2012= 0    		
		datos = []
    		datos.append(newest)
    		est = newest
finally:
    datos.append(accidentes2014+1)
    datos.append(accidentes2013)
    datos.append(accidentes2012)
    estados.append(datos)
    f.close()

f = open("RoadSafetyPivot1.csv",'wt')
try:
    writer = csv.writer(f)
    writer.writerow(('Zona','Accidentes 2014', 'Accidentes 2013', 'Accidentes 2012'))
    for i in range(1,len(estados)):
        writer.writerow((estados[i]))
finally:
    f.close()
